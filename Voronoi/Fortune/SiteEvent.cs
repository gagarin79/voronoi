﻿namespace Voronoi.Fortune
{
	/// <summary>
	/// Represents a site event within the event queue. During site
	/// events a new arc segnment appears on the beach line.
	/// </summary>
	/// <typeparam name="T">User-specified site data.</typeparam>
	internal class SiteEvent<T> : Event
	{
		/// <summary>
		/// The site that caused this site event.
		/// </summary>
		public Site<T> Site { get; private set; }

		/// <summary>
		/// Constructs a new SiteEvent object.
		/// </summary>
		/// <param name="site">The site that caused this site event.</param>
		public SiteEvent(Site<T> site) : base(site.Y)
		{
			Site = site;
		}
	}
}
