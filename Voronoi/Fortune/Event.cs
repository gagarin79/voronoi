﻿namespace Voronoi.Fortune
{
	/// <summary>
	/// Common base class for site and circle events to
	/// allow them to be placed into an event queue based
	/// on their y coordinate.
	/// </summary>
	internal abstract class Event
	{
		/// <summary>
		/// Y coordinate at which this event occurs.
		/// </summary>
		public double Y { get; private set; }

		/// <summary>
		/// Constructs a new Event object.
		/// </summary>
		public Event(double y)
		{
			Y = y;
		}
	}
}
