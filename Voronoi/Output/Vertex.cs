﻿namespace Voronoi.Output
{
	/// <summary>
	/// A vertex component of a doubly-connected edge list.
	/// </summary>
	/// <typeparam name="T">Site user data type.</typeparam>
	public class Vertex<T>
	{
		/// <summary>
		/// One of possibly many half edges with this vertex
		/// as its origin.
		/// </summary>
		public HalfEdge<T> Leaving { get; set; }
	}
}
