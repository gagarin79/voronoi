﻿using System;
using NUnit.Framework;
using Voronoi.Fortune;

namespace Voronoi.Test
{
	[TestFixture]
	public class CalculationsTest
	{
		private const int Count = 100000;

		[Test(Description = "Determines whether points that are specifically generated to be collinear are so judged.")]
		public void AreCollinearReturnsTrue()
		{
			Random rand = new Random();
			const double InitialRange = 20000.0;
			// Generate lots of collinear triples.
			for (int i = 0; i < Count; i++)
			{
				// Start with two points with coordinates in the range [-InitialRange, InitialRange].
				Point a = new Point(
					(rand.NextDouble() * 2.0 - 1.0) * InitialRange, 
					(rand.NextDouble() * 2.0 - 1.0) * InitialRange);
				Point b = new Point(
					(rand.NextDouble() * 2.0 - 1.0) * InitialRange,
					(rand.NextDouble() * 2.0 - 1.0) * InitialRange);
				// Create a collinear third point by adding b to a.
				Point c = b + (b - a);
				Assert.True(Calculations.AreCollinear(a, b, c),
					"Points {0}, {1}, {2} are collinear but Calculations.AreCollinear() returned false.",
					a,
					b,
					c);
			}
		}

		[Test(Description = "Determines whether points that are specifically generated to be noncollinear are so judged.")]
		public void AreCollinearReturnsFalse()
		{
			Random rand = new Random();
			const double InitialRange = 20000.0;
			// Generate lots of noncollinear triples.
			for (int i = 0; i < Count; i++)
			{
				// Start with two points with coordinates in the range [-InitialRange, InitialRange].
				Point a = new Point(
					(rand.NextDouble() * 2.0 - 1.0) * InitialRange,
					(rand.NextDouble() * 2.0 - 1.0) * InitialRange);
				Point b = new Point(
					(rand.NextDouble() * 2.0 - 1.0) * InitialRange,
					(rand.NextDouble() * 2.0 - 1.0) * InitialRange);
				// Create a collinear third point by adding b to a.
				Point c = a + new Point(b.X, -b.Y);
				Assert.False(Calculations.AreCollinear(a, b, c),
					"Points {0}, {1}, {2} are note collinear but Calculations.AreCollinear() returned true.",
					a,
					b,
					c);
			}
		}
	}
}
