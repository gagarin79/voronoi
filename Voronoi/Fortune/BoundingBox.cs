﻿namespace Voronoi.Fortune
{
	/// <summary>
	/// A rectangular bounding box to attatch leftover
	/// edges to.
	/// </summary>
	public class BoundingBox
	{
		private double x, y, width, height;

		/// <summary>
		/// Constructs a BoundingBox object over a rectangle.
		/// </summary>
		/// <param name="x">Minimum x coordinate.</param>
		/// <param name="y">Minimum y coordinate.</param>
		/// <param name="width">The rectangle's width.</param>
		/// <param name="height">The rectangle's height.</param>
		public BoundingBox(double x, double y, double width, double height)
		{
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}

		/// <summary>
		/// Tests whether a site is contained within this bounding box.
		/// Sites on the edge of the bounding box are considered outside.
		/// </summary>
		/// <typeparam name="T">User specified site data type.</typeparam>
		/// <param name="site">Site to test for containment.</param>
		/// <returns>True if the site is within the edge of the bounding
		/// box, false if it is on or outside the edge of the bounding box.</returns>
		internal bool IsWithin<T>(Site<T> site)
		{
			return site.X > x && site.X < x + width - 1.0
				&& site.Y > y && site.Y < y + height - 1.0;
		}
	}
}
