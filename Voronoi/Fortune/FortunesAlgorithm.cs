﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Voronoi.Output;

namespace Voronoi.Fortune
{
	/// <summary>
	/// An implementation of Fortune's algorithm for creating Voronoi diagrams.
	/// </summary>
	/// <remarks>Mr. Fortune's original whitepaper can be found at http://dl.acm.org/citation.cfm?id=10549.
	/// I primarily depended on the book Computational Geometry by de Berg, Gheong, van Kreveld,
	/// and Overmars for explanation of the algorithm.</remarks>
	public class FortunesAlgorithm : IVoronoiAlgorithm
	{
		#region Fields

		// Config
		private double padding;
		private bool usePadding;
		private BoundingBox boundingBox;

		// Data structures.
		private PriorityQueue<Event> eventQueue;
		private BeachTreeNode beachRoot;
		
		#endregion

		#region Properties

		/// <summary>
		/// Sets the padding to use if UsePadding is true.
		/// </summary>
		public double Padding
		{
			set { padding = value; }
		}

		/// <summary>
		/// Controls whether or not padding is applied to 
		/// the bounding box.
		/// </summary>
		public bool UsePadding
		{
			set { usePadding = value; }
		}

		/// <summary>
		/// The bounding box used to cull input sites. If
		/// this is null all sites are culled.
		/// </summary>
		public BoundingBox BoundingBox
		{
			set { boundingBox = value; }
		}

		#endregion

		#region Lifetime

		/// <summary>
		/// Constructs a FortunesAlgorithm object.
		/// </summary>
		/// <param name="boundingBox">The initial bounding box.</param>
		public FortunesAlgorithm(BoundingBox boundingBox)
		{
			padding = 0;
			usePadding = false;
			this.boundingBox = boundingBox;
		}

		#endregion

		#region Implementation of IVoronoiAlgorithm

		public DCEL<T> ComputeVoronoi<T>(IEnumerable<Site<T>> sites)
		{
			if(sites == null)
				return new DCEL<T>();
			if(boundingBox == null)
				return new DCEL<T>();

			// Can't have result declared with other fields because then the
			// class would have to be type parameterized.
			DCEL<T> result = new DCEL<T>();
			Initialize(sites);

			var nextEvent = eventQueue.Remove();
			while (nextEvent != null)
			{
				var siteEvent = nextEvent as SiteEvent<T>;
				if (siteEvent != null)
					HandleSiteEvent(siteEvent);
				else 
					HandleCircleEvent(nextEvent as CircleEvent<T>);
			}

			CleanUp();
			return result;
		}

		#endregion

		#region Private Methods

		private void Initialize<T>(IEnumerable<Site<T>> sites)
		{
			// Events with smaller y coordinate have higher priorities.
			eventQueue = new PriorityQueue<Event>((i, j) => (i < j));
			// Empty status structure.
			beachRoot = null;
			// Cull out of bounds sites.
			foreach (var site in sites.Where(s => boundingBox.IsWithin(s)))
				eventQueue.Add(new SiteEvent<T>(site), site.Y);
		}

		private void CleanUp()
		{
			beachRoot = null;
			eventQueue = null;
		}

		private void HandleSiteEvent<T>(SiteEvent<T> siteEvent)
		{
			if (beachRoot == null)
			{
				beachRoot = new ArcNode<T>(siteEvent.Site);
				return;
			}

			double sweepLine = siteEvent.Y;

			// Search in the beach tree for the arc vertically above
			// the site.
			BeachTreeNode current = beachRoot, parent = null;
			bool left = false;
			while (!current.IsArcNode)
			{
				double breakpointX = (current as BreakpointNode<T>).ComputeBreakpointX(sweepLine);
				if (siteEvent.Site.X < breakpointX)
				{
					parent = current;
					current = current.LeftChild;
					left = true;
				}
				else
				{
					parent = current;
					current = current.RightChild;
					left = false;

				}
				Debug.Assert(current != null);
			}

			var arcToSplit = current as ArcNode<T>;

			// If the leaf representing the arc to split has
			// a circle event, then this circle event is a false 
			// alarm and it must be deleted from the event queue.
			if (arcToSplit.DisappearEvent != null)
				eventQueue.Delete(arcToSplit.DisappearEvent);

			// Replace the leaf representing the arc to split
			// with a subtree having three leaves. The middle
			// leaf stores the new site and the other two leaves
			// stores the site that was originally stored in the
			// arc to split. The two internal breakpoint nodes
			// receive the site pairs (original, new) and (new, original).
			// NOTE 1: There are two ways to add the new subtree
			// with three leaves and two internal nodes. For now
			// I just choose the one that expands to the left.
			// NOTE 2: If I was implementing a balanced beach line
			// tree, I would need to do rebalancing operations
			// after this, but I'm not so I don't have to.
			var upperRightBreakpoint = new BreakpointNode<T>(siteEvent.Site, arcToSplit.Site);
			var lowerLeftBreakpoint = new BreakpointNode<T>(arcToSplit.Site, siteEvent.Site);
			var leftArc = new ArcNode<T>(arcToSplit.Site);
			var middleArc = new ArcNode<T>(siteEvent.Site);
			var rightArc = new ArcNode<T>(arcToSplit.Site);
			// Link the new subtree together.
			upperRightBreakpoint.LeftChild = lowerLeftBreakpoint;
			upperRightBreakpoint.RightChild = rightArc;
			lowerLeftBreakpoint.LeftChild = leftArc;
			lowerLeftBreakpoint.RightChild = middleArc;
			// Attach the subtree to the original.
			if (parent == null)
				beachRoot = upperRightBreakpoint;
			else
			{
				if (left)
					parent.LeftChild = upperRightBreakpoint;
				else
					parent.RightChild = upperRightBreakpoint;
			}
			// If this was C++ we would delete the arcToSplit here.
		}

		private void HandleCircleEvent<T>(CircleEvent<T> circleEvent)
		{
			
		}

		#endregion
	}
}
