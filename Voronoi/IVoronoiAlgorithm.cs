﻿using System.Collections.Generic;
using Voronoi.Fortune;
using Voronoi.Output;

namespace Voronoi
{
	/// <summary>
	/// Provides Voronoi diagram computation service.
	/// </summary>
	public interface IVoronoiAlgorithm
	{
		/// <summary>
		/// Computes a Voronoi diagram from a list of input sites.
		/// </summary>
		/// <typeparam name="T">User-specified site data type.</typeparam>
		/// <param name="sites">The sites that will be used to generate
		/// a Voronoi diagram.</param>
		/// <returns>The Voronoi diagram computed from the given sites,
		/// represented as a valid doubly-connected edge list.</returns>
		DCEL<T> ComputeVoronoi<T>(IEnumerable<Site<T>> sites);
	}
}
