﻿namespace Voronoi.Output
{
	/// <summary>
	/// A half edge component of a doubly-connected edge list.
	/// </summary>
	/// <typeparam name="T">Site user data type.</typeparam>
	public class HalfEdge<T>
	{
		/// <summary>
		/// The half edge to the "right" of this half edge that completes the full edge.
		/// </summary>
		public HalfEdge<T> Twin { get; set; }

		/// <summary>
		/// The half edge that starts from this edge's twin's origin and ends at the 
		/// next vertex bordering this edge's face, traveling counterclockwise around the boundary.
		/// </summary>
		public HalfEdge<T> Next { get; set; }

		/// <summary>
		/// The face that this half edge borders and is oriented toward. This face is 
		/// to the "left" of the edge.
		/// </summary>
		public Face<T> Face { get; set; }

		/// <summary>
		/// The vertex from which this half edge starts.
		/// </summary>
		public Vertex<T> Origin { get; set; }
	}
}
