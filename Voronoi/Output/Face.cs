﻿namespace Voronoi.Output
{
	/// <summary>
	/// A face component of a doubly connected edge list.
	/// </summary>
	/// <typeparam name="T">Site user data type.</typeparam>
	public class Face<T>
	{
		/// <summary>
		/// User-specified data taken from the input site that defines this face.
		/// </summary>
		public T Data { get; private set; }

		/// <summary>
		/// One of the inward-facing half edges that border this face.
		/// </summary>
		public HalfEdge<T> Edge { get; set; }

		/// <summary>
		/// Constructs a Face object.
		/// </summary>
		/// <param name="data">User-specified data taken from the
		/// input site that defines this face.</param>
		public Face(T data)
		{
			Data = data;
		}
	}
}
