﻿using System.Collections;
using System.Collections.Generic;

namespace Voronoi.Output
{
	/// <summary>
	/// A doubly-connected edge list data structure with
	/// generic face data.
	/// </summary>
	/// <typeparam name="T">Site user data type.</typeparam>
	public class DCEL<T> : IEnumerable<Face<T>>, IEnumerable<HalfEdge<T>>, IEnumerable<Vertex<T>>
	{
		private readonly Face<T> infinite;
		private readonly List<Face<T>> faces;
		private readonly List<HalfEdge<T>> edges;
		private readonly List<Vertex<T>> vertices;

		public DCEL()
		{
			infinite = new Face<T>(default(T));
			faces = new List<Face<T>>();
			edges = new List<HalfEdge<T>>();
			vertices = new List<Vertex<T>>();
		}

		#region Insertion

		internal void Add(Face<T> face)
		{
			faces.Add(face);
		}

		internal void Add(HalfEdge<T> edge)
		{
			edges.Add(edge);
		}

		internal void Add(Vertex<T> vertex)
		{
			vertices.Add(vertex);
		}

		#endregion

		#region Implementation of IEnumerable

		IEnumerator<Vertex<T>> IEnumerable<Vertex<T>>.GetEnumerator()
		{
			return vertices.GetEnumerator();
		}

		IEnumerator<HalfEdge<T>> IEnumerable<HalfEdge<T>>.GetEnumerator()
		{
			return edges.GetEnumerator();
		}

		IEnumerator<Face<T>> IEnumerable<Face<T>>.GetEnumerator()
		{
			return faces.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return faces.GetEnumerator();
		}

		#endregion
	}
}
