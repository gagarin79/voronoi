﻿using System;

namespace Voronoi.Fortune
{
	/// <summary>
	/// A point with two coordinates.
	/// </summary>
	internal struct Point
	{
		/// <summary>
		/// The point's x coordinate.
		/// </summary>
		public readonly double X;

		/// <summary>
		/// The point's y coordinate.
		/// </summary>
		public readonly double Y;

		/// <summary>
		/// Constructs a Point object.
		/// </summary>
		/// <param name="x">The point's x coordinate.</param>
		/// <param name="y">The point's y coordiante.</param>
		public Point(double x, double y)
		{
			X = x;
			Y = y;
		}

		/// <summary>
		/// Indicates whether this instance and a specified object are equal.
		/// </summary>
		/// <returns>
		/// true if <paramref name="obj"/> and this instance are the same type and represent the same value; otherwise, false.
		/// </returns>
		/// <param name="obj">Another object to compare to. </param><filterpriority>2</filterpriority>
		public override bool Equals(object obj)
		{
			if (obj is Point)
			{
				return this == (Point)obj;
			}
			return false;
		}

		public bool Equals(Point other)
		{
			return this == other;
		}

		public override string ToString()
		{
			return string.Format("[{0}, {1}]", X, Y);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode() ^ X.GetHashCode() ^ Y.GetHashCode();
		}
		
		#region Operations

		public static bool operator==(Point a, Point b)
		{
			return a.X == b.X && a.Y == b.Y;
		}

		public static bool operator!=(Point a, Point b)
		{
			return a.X != b.X || a.Y != b.Y;
		}

		public static Point operator+(Point a, Point b)
		{
			return new Point(a.X + b.X, a.Y + b.Y);
		}

		public static Point operator-(Point a, Point b)
		{
			return new Point(a.X - b.X, a.Y - b.Y);
		}

		public static double Distance(Point a, Point b)
		{
			return Math.Sqrt((a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y));
		}

		#endregion
	}
}
