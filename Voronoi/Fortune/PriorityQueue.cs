﻿using System.Diagnostics;

namespace Voronoi.Fortune
{
	/// <summary>
	/// A priority queue with integer priorities.
	/// </summary>
	/// <typeparam name="T">The type of data stored in the priority queue.</typeparam>
	internal class PriorityQueue<T>
	{
		#region Type Declarations

		private class Element
		{
			public T Data { get; private set; }
			public int Priority { get; private set; }

			public Element(T data, int priority)
			{
				Data = data;
				Priority = priority;
			}

			public override string ToString()
			{
				return string.Format("[{0}, priority={1}", Data, Priority);
			}
		}

		/// <summary>
		/// Determines which of two integer priority values comes before the other.
		/// </summary>
		/// <param name="first">The first priority value.</param>
		/// <param name="second">The second priority value.</param>
		/// <returns>True if the first priority value comes before the second,
		/// false otherwise.</returns>
		public delegate bool Prioritizer(int first, int second);

		#endregion

		// The priority queue is a binary tree implemented as an array.
		private Element[] array; 
		// Index of the first open
		private int firstOpen;
		// Prioritizes elements.
		private Prioritizer prioritizer;

		/// <summary>
		/// Constructs a PriorityQueue object with the
		/// default setting that smaller integer priority 
		/// values come before larger ones..
		/// </summary>
		public PriorityQueue()
		{
			array = new Element[16];
			firstOpen = 0;
			prioritizer = (i, j) => i < j;
		}

		/// <summary>
		/// Constructs a PriorityQueue object with
		/// a user-defined prioritizer.
		/// </summary>
		/// <param name="prioritizer">Determines which priorities
		/// come first.</param>
		public PriorityQueue(Prioritizer prioritizer)
		{
			array = new Element[16];
			firstOpen = 0;
			this.prioritizer = prioritizer;
		}

		#region Priority Queue Operations

		/// <summary>
		/// Adds an element to the priority queue.
		/// </summary>
		/// <param name="data">The element to add.</param>
		/// <param name="priority">The element's priority. Lower 
		/// integer values have higher priority.</param>
		public void Add(T data, int priority)
		{
			if(firstOpen >= array.Length)
				Resize(firstOpen * 2);
			array[firstOpen++] = new Element(data, priority);
			WalkUp(firstOpen - 1);
		}

		/// <summary>
		/// Returns but does not remove the element with the highest priority.
		/// </summary>
		/// <returns>The element with the highest priority, or the default()
		/// value for the element type if the queue is empty.</returns>
		public T Peek()
		{
			return firstOpen == 0 ? default(T) : array[0].Data;
		}

		/// <summary>
		/// Returns and removes the element with the highest priority.
		/// </summary>
		/// <returns>The element with the highest priority, or the default()
		/// value for the element type if the queue is empty.</returns>
		public T Remove()
		{
			if (firstOpen == 0)
				return default(T);
			var data = array[0].Data;
			firstOpen--;
			array[0] = array[firstOpen];
			array[firstOpen] = null;
			WalkDown(0);
			return data;
		}

		/// <summary>
		/// Deletes a specific item from the priority queue.
		/// </summary>
		/// <remarks>Because a priority queue is not a search tree,
		/// this is an O(n) operation.</remarks>
		/// <param name="item"></param>
		public void Delete(T item)
		{
			for (int i = 0; i < firstOpen; i++)
			{
				if (ReferenceEquals(array[i].Data, item))
				{
					array[i] = array[firstOpen - 1];
					array[firstOpen - 1] = null;
					firstOpen--;
					WalkUp(i);
					WalkDown(i);
					return;
				}
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Resizes the binary tree array to a new size.
		/// </summary>
		/// <param name="newSize">The new size of the array.</param>
		private void Resize(int newSize)
		{
			// Only resize to bigger length.
			Debug.Assert(newSize > array.Length);
			var newArray = new Element[newSize];
			// Copy all preexisting elements.
			for (int i = 0; i < array.Length; i++)
				newArray[i] = array[i];
			// Set the rest of the new array to null.
			for (int i = array.Length; i < newArray.Length; i++)
				newArray[i] = null;
			array = newArray;
		}

		/// <summary>
		/// Walks an element up the tree until it reaches
		/// its appropriate place based on its priority.
		/// </summary>
		/// <param name="startIndex">The array index of the
		/// element to walk up the tree.</param>
		/// <returns>The array index of the element's final position.</returns>
		private int WalkUp(int startIndex)
		{
			Debug.Assert(startIndex < firstOpen);
			Debug.Assert(startIndex >= 0);
			// Parent calculation is actually floor((pos - 1) / 2), but
			// positive integer division takes care of that for us.
			int pos = startIndex, parent = (pos - 1) / 2;
			while (pos > 0 && prioritizer(array[pos].Priority, array[parent].Priority))
			{
				Swap(ref array[parent], ref array[pos]);
				pos = parent;
				parent = (pos - 1) / 2;
			}
			return pos;
		}

		/// <summary>
		/// Walks an element down the tree until it reaches
		/// its appropriate place based on its priority.
		/// </summary>
		/// <param name="startIndex">The array index of the
		/// element to walk down the tree.</param>
		/// <returns>The array index of the element's final position.</returns>
		private int WalkDown(int startIndex)
		{
			Debug.Assert(startIndex >= 0);
			int pos = startIndex,
				left = pos * 2 + 1,
				right = pos * 2 + 2,
				// Remember lesser integer ==> higher priority.
				higherPriority = right < firstOpen
					? (prioritizer(array[left].Priority, array[right].Priority) ? left : right)
					: left;
			while (left < firstOpen && !prioritizer(array[pos].Priority, array[higherPriority].Priority))
			{
				Swap(ref array[pos], ref array[higherPriority]);
				pos = higherPriority;
				left = 2 * pos + 1;
				right = left + 1;
				higherPriority = right < firstOpen
					? (prioritizer(array[left].Priority, array[right].Priority) ? left : right)
					: left;
			}
			return pos;
		}

		/// <summary>
		/// Swaps. Amazing.
		/// </summary>
		private void Swap<E>(ref E a, ref E b)
		{
			E temp = a;
			a = b;
			b = temp;
		}

		#endregion
	}
}
