﻿namespace Voronoi.Fortune
{
	/// <summary>
	/// Represents an input site with user-specified data.
	/// </summary>
	public struct Site<T>
	{
		private T data;

		/// <summary>
		/// User-specified data that will be transferred
		/// to the face corresponding to this site in
		/// the output doubly connected edge list.
		/// </summary>
		public T Data
		{
			get { return data; }
		}

		/// <summary>
		/// The site's x coordinate.
		/// </summary>
		public readonly int X;

		/// <summary>
		/// The site's y coordinate.
		/// </summary>
		public readonly int Y;

		public Site(int x, int y, T data)
		{
			X = x;
			Y = y;
			this.data = data;
		}
	}
}
