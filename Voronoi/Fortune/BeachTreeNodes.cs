﻿using System;
using System.Diagnostics;
using Voronoi.Output;

namespace Voronoi.Fortune
{
	internal abstract class BeachTreeNode
	{
		/// <summary>
		/// Left subtree.
		/// </summary>
		public BeachTreeNode LeftChild { get; set; }

		/// <summary>
		/// Right subtree.
		/// </summary>
		public BeachTreeNode RightChild { get; set; }

		/// <summary>
		/// Returns true if this is an ArcNode, false if this
		/// is a BreakpointNode.
		/// </summary>
		public abstract bool IsArcNode { get; }
	}

	/// <summary>
	/// A leaf node in the beach line binary tree
	/// representing an arc on the beach line.
	/// </summary>
	internal class ArcNode<T> : BeachTreeNode
	{
		/// <summary>
		/// The site that defines the arc this ArcNode represents.
		/// </summary>
		public readonly Site<T> Site;

		/// <summary>
		/// The circle event in which the arc this ArcNode represents
		/// will disappear. This is null if no circle event exists
		/// where this arc will disappear or such a circle event
		/// has not been detected yet.
		/// </summary>
		public CircleEvent<T> DisappearEvent;

		public override bool IsArcNode
		{
			get { return true; }
		}

		/// <summary>
		/// Constructs an ArcNode object.
		/// </summary>
		/// <param name="site">The site that defines the arc this
		/// ArcNode represents.</param>
		public ArcNode(Site<T> site)
		{
			Site = site;
			DisappearEvent = null;
		}
	}

	/// <summary>
	/// An internal node in the beach line binary
	/// tree representing a breakpoint on the 
	/// beach line.
	/// </summary>
	/// <typeparam name="T">User-specified site data.</typeparam>
	internal class BreakpointNode<T> : BeachTreeNode
	{
		/// <summary>
		/// The site defining the parabola left of the breakpoint.
		/// </summary>
		public readonly Site<T> Left;

		/// <summary>
		/// The site defining the parabola right of the breakpoint.
		/// </summary>
		public readonly Site<T> Right;

		/// <summary>
		/// One of the half-edges of the edge being traced out by
		/// the breakpoint represented by this BreakpointNode.
		/// </summary>
		public HalfEdge<T> HalfEdge;

		public override bool IsArcNode
		{
			get { return false; }
		}

		/// <summary>
		/// Constructs a BreakpointNode object.
		/// </summary>
		/// <param name="left">The site defining the parabola left of the breakpoint.</param>
		/// <param name="right">The site defining the parabola right of the breakpoint.</param>
		public BreakpointNode(Site<T> left, Site<T> right)
		{
			Left = left;
			Right = right;
		}

		/// <summary>
		/// Computes the x coordinate of the breakpoint represented by this node
		/// from the two sites on either side of the breakpoint and the current
		/// y position of the sweep line.
		/// </summary>
		/// <param name="sweepLineY">The y coordinate of the sweep line.</param>
		/// <returns>The x coordinate of the breakpoint.</returns>
		public double ComputeBreakpointX(double sweepLineY)
		{
			Debug.Assert(sweepLineY != Left.Y && sweepLineY != Right.Y);

			Point leftVertex = new Point(Left.X, (Left.Y + sweepLineY) / 2.0);
			Point rightVertex = new Point(Right.X, (Right.Y + sweepLineY) / 2.0);
			double leftP = leftVertex.Y - sweepLineY;
			double rightP = rightVertex.Y - sweepLineY;
			// Get coefficients for parabola formulas.
			double leftA = 1.0 / (4.0 * leftP),
				leftB = -leftVertex.X / (2.0 * leftP),
				leftC = leftVertex.Y + leftVertex.X * leftVertex.X / (4.0 * leftP);
			double rightA = 1.0 / (4.0 * rightP),
				rightB = -rightVertex.X / (2.0 * rightP),
				rightC = rightVertex.Y + rightVertex.X * rightVertex.X / (4.0 * rightP);
			// Get quadratic equation variables.
			double quadraticA = leftA - rightA,
				quadraticB = leftB - rightB,
				quadraticC = leftC - rightC;
			if (quadraticA == 0)
				throw new InvalidOperationException("quadraticA was 0, no intersection.");
			double determinant = quadraticB * quadraticB - 4.0 * quadraticA * quadraticC;
			if(determinant < 0)
				throw new InvalidOperationException("determinant was < 0, no intersection.");
			double sqrtDeterminant = Math.Sqrt(determinant);
			double intercept1 = (-quadraticB + sqrtDeterminant) / (2.0 * quadraticA),
				intercept2 = (-quadraticB - sqrtDeterminant) / (2.0 * quadraticA);
			if (intercept1 > Left.X && intercept1 < Right.X)
				return intercept1;
			if (intercept2 > Left.X && intercept2 < Right.X)
				return intercept2;
			throw new InvalidOperationException("Neither intercept x-coordinate was between the two sites.");
		}
	}
}
