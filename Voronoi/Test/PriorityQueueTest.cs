﻿using System;
using NUnit.Framework;
using Voronoi.Fortune;

namespace Voronoi.Test
{
	[TestFixture]
	public class PriorityQueueTest
	{
		private const int Max = 100;

		[Test]
		public void PeekDoesNotChange()
		{
			var pq = new PriorityQueue<int>();
			foreach (var r in MakeRandom(0, Max - 1)) 
				pq.Add(r, r);
			for (int expected = 0; expected < Max; expected++)
			{
				Assert.AreEqual(expected, pq.Peek());
				Assert.AreEqual(expected, pq.Remove());
			}
		}

		[Test]
		public void IncreasingAddition()
		{
			var pq = new PriorityQueue<int>();
			for (int priority = 0; priority < Max; priority++)
				pq.Add(priority, priority);
			for (int expected = 0; expected < Max; expected++)
				Assert.AreEqual(expected, pq.Remove());
		}

		[Test]
		public void DecreasingAdditions()
		{
			var pq = new PriorityQueue<int>();
			for (int priority = Max - 1; priority >= 0; priority--)
				pq.Add(priority, priority);
			for (int expected = 0; expected < Max; expected++)
				Assert.AreEqual(expected, pq.Remove());
		}

		[Test]
		public void RandomAddition()
		{
			var pq = new PriorityQueue<int>();
			foreach (int i in MakeRandom(0, Max - 1))
				pq.Add(i, i);
			for (int i = 0; i < Max; i++)
			{
				Assert.AreEqual(i, pq.Remove());
			}
		}

		[Test]
		public void DuplicateRemoval()
		{
			var pq = new PriorityQueue<int>();
			foreach (int i in MakeRandom(0, Max - 1))
			{
				pq.Add(i, i);
				pq.Add(i, i);
			}
			for (int expected = 0; expected < Max; expected++)
			{
				Assert.AreEqual(expected, pq.Remove());
				Assert.AreEqual(expected, pq.Remove());
			}
		}

		private int[] MakeRandom(int start, int end)
		{
			int count = end - start + 1;
			int[] array = new int[count];
			var rand = new Random();
			for (int i = 0; i < count; i++)
				array[i] = i;
			for (int i = 0; i < array.Length; i++)
			{
				int source = rand.Next(count--) + i;
				int temp = array[source];
				array[source] = array[i];
				array[i] = temp;
			}
			return array;
		}
	}
}
