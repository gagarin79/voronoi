﻿using System;
using System.Diagnostics;

namespace Voronoi.Fortune
{
	/// <summary>
	/// Storage class for calculations necessary for Fortune's
	/// algorithm that don't fit into other classes.
	/// </summary>
	internal static class Calculations
	{
		/// <summary>
		/// Computes the center of a circle that passes through three points.
		/// The points must not be collinear.
		/// </summary>
		/// <param name="a">The first point on the circle.</param>
		/// <param name="b">The second point on the circle.</param>
		/// <param name="c">The third point on the circle.</param>
		/// <returns>The coordinate of the </returns>
		public static Point GetCircleCenter(Point a, Point b, Point c)
		{
			Debug.Assert(!AreCollinear(a, b, c), "Cannot compute center of circle of three collinear points.");

			// h term's denominator must not be zero.
			if (a.X == b.X)
			{
				// Rotate the points. This will mean that the new
				// a.X will not equal the new b.X - unless the points are
				// collinear, of course.
				var temp = a;
				a = b;
				b = c;
				c = temp;
			}
			double hDenom = (2 * (a.X - b.X));
			Debug.Assert(hDenom != 0, "h term's denominator was zero.", "Points were {0}, {1}, {2}.", a, b, c);

			double kDenom = (2.0 * (a.X * (b.Y - c.Y) - a.Y * (b.X - c.X) + b.X * c.Y - b.Y * c.X));
			Debug.Assert(kDenom != 0, "k term's denominator was zero.", "Points were {0}, {1}, {2}.", a, b, c);
			
			double k =
				-(a.X * a.X * (b.X - c.X) - a.X * (b.X * b.X + b.Y * b.Y - c.X * c.X - c.Y * c.Y) + a.Y * a.Y * (b.X - c.X)
					+ b.X * b.X * c.X - b.X * (c.X * c.X + c.Y * c.Y) + b.Y * b.Y * c.X)
						/ kDenom;
			double h = -(2.0 * k * (a.Y - b.Y) - a.X * a.X - a.Y * a.Y + b.X * b.X + b.Y * b.Y) / hDenom;
			return new Point(h, k);
		}

		/// <summary>
		/// Determines whether three points are collinear.
		/// </summary>
		/// <param name="a">The first point.</param>
		/// <param name="b">The second point.</param>
		/// <param name="c">The third point.</param>
		/// <returns>True if the points are collinear, false otherwise.</returns>
		public static bool AreCollinear(Point a, Point b, Point c)
		{
			const double Tolerance = 0.000001;
			//return a.X * (b.Y - c.Y) + b.X * (c.Y - a.Y) + c.X * (a.Y - b.Y) == 0;
			return Math.Abs(a.X * (b.Y - c.Y) + b.X * (c.Y - a.Y) + c.X * (a.Y - b.Y)) < Tolerance;
		}
	}
}
