﻿namespace Voronoi.Fortune
{
	/// <summary>
	/// Represents a circle event in the event queue. During circle
	/// events an existing arc drops out of the beach line, resulting
	/// in the discovery of a new vertex of the doubly-connected edge list.
	/// </summary>
	/// <typeparam name="T">User-specified type data.</typeparam>
	internal class CircleEvent<T> : Event
	{
		/// <summary>
		/// Site that defines the arc that will disappear from the
		/// beach line as a result of this circle event.
		/// </summary>
		public Site<T> MiddleSite { get; private set; }

		/// <summary>
		/// Constructs a CircleEvent object.
		/// </summary>
		/// <param name="middleSite">The site that defines the arc
		/// that will disappear from the beach line as a result
		/// of this circle event.</param>
		/// <param name="y">The greatest y coordinate of the circle.</param>
		public CircleEvent(Site<T> middleSite, double y) : base(y)
		{
			MiddleSite = middleSite;
		}
	}
}
